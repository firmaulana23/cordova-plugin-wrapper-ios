#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface VerihubsCoreC : NSObject
    
- (void) initialization;
- (void) deinitialization;
- (int*) detectFace: (UIImage *) image;
- (float*) getLandmarks: (UIImage *) image rectFace:(int*) rectFace flag:(int) flag;
- (float*) getSpoof: (UIImage *) image flag:(int) flag;
- (int) detectAttribute: (UIImage *) image rectFace:(int*) rectFace;
- (double) detectBlurSelfie: (UIImage *) image rectFace:(int*) rectFace;
- (double) detectDarkSelfie: (UIImage *) image rectFace:(int*) rectFace;
- (bool) detectGraySelfie: (UIImage *) image rectFace:(int*) rectFace;
- (double) detectBlurID: (UIImage *) image rectID:(int*) rectID;
- (double) detectDarkID: (UIImage *) image rectID:(int*) rectID;
- (UIImage *) analysis: (UIImage *) image;
- (UIImage *) getImage: (UIImage *) image;
    
@end
