//
//  VerihubsLayout.h
//  layout
//
//  Created by Verihubs 002 on 28/09/21.
//

#ifndef VerihubsLayout_h
#define VerihubsLayout_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface VerihubsLayout : NSObject

- (UIImage*) resizeMat: (UIImage *) image width:(int) width height:(int) height;
- (UIImage*) printBox: (UIImage *) image x:(int) x y:(int) y w:(int) w h:(int) h ratio:(float) ratio;

@end

#endif /* VerihubsLayout_h */
