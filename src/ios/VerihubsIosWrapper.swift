
import Foundation
import bcad
import UIKit

@objc(VerihubsIosWrapper)
public class VerihubsIosWrapper :  CDVPlugin , VerihubsDelegate{

  var verisdk: VerihubsSDK_BCAD!
  var resp: String!
  var instruction_count: Int!
  var timeout: Int!
  var commandId: String!
  var string_parameters: [AnyHashable : Any] = [:]
  var asset_parameters: [AnyHashable : Any] = [:]
  var custom_instructions: [Int32] = []
  var attributes_check: [Bool] = []

    public func setResponse(response_status: String)
    {
        self.resp = response_status
    }
    
  @objc
  func initClass(_ command: CDVInvokedUrlCommand) {

    verisdk = VerihubsSDK_BCAD()
    var pluginResult:CDVPluginResult

    pluginResult = CDVPluginResult.init(status: CDVCommandStatus_OK, messageAs: "Class has been initialized")
    self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
  }


  @objc
  func verifyLiveness(_ command: CDVInvokedUrlCommand) {

    self.instruction_count = command.arguments[0] as! Int?
    self.timeout = command.argument(at: 1) as! Int?
    self.attributes_check = command.argument(at: 3) as! [Bool]
    self.custom_instructions = command.argument(at: 2) as! [Int32]
    self.commandId = command.callbackId
    //liveness tutorial -- texts
    VerihubsString_BCAD.getInstance().setFirstTutorialText(text: "Pastikan wajahmu terlihat jelas \n-Lepaskan kacamata\n-Lepaskan topi \n-Lepaskan masker")
    VerihubsString_BCAD.getInstance().setFirstTutorialButtonText(text: "Oke")
    VerihubsString_BCAD.getInstance().setSecondTutorialText(text: "Pegang handphone setinggi mata")
    VerihubsString_BCAD.getInstance().setSecondTutorialButtonText(text: "Saya sudah siap")
    VerihubsString_BCAD.getInstance().setThirdTutorialText(text: "Pastikan wajahmu berada dalam lingkaran")
    
    
    //liveness tutorial -- color
    VerihubsAsset_BCAD.getInstance().setTutorialBackgroundColor(color: .white)
    VerihubsAsset_BCAD.getInstance().setFirstTutorialButtonColor(color: .blue)
    VerihubsAsset_BCAD.getInstance().setSecondTutorialButtonColor(color: .blue)
    VerihubsAsset_BCAD.getInstance().setButtonTutorialBackgroundColor(color: .blue)
    
    
    //liveness tutorial -- text color
    VerihubsAsset_BCAD.getInstance().setTutorialTextColor(color: .black)
    VerihubsAsset_BCAD.getInstance().setTutorialButtonTextColor(color: .white)
    
    //liveness tutorial --assets
    VerihubsAsset_BCAD.getInstance().setFirstTutorialAsset(imageName: "mask-hat-glasses")
    VerihubsAsset_BCAD.getInstance().setSecondTutorialAsset(imageName: "lea-eye-level")
    VerihubsAsset_BCAD.getInstance().setThirdTutorialAsset(imageName: "lea-face-circle")
    
    //liveness layout -- color
    VerihubsAsset_BCAD.getInstance().setLivenessBackgroundColor(color: .white)
    VerihubsAsset_BCAD.getInstance().setLivenessInvertedCircleColor(color: .gray)
    VerihubsAsset_BCAD.getInstance().setLivenessProgressColor(color: .red)
    
    //liveness -- text color
    VerihubsAsset_BCAD.getInstance().setLivenessInstructionsTextColor(color: .blue)
    VerihubsAsset_BCAD.getInstance().setLivenessWarningTextColor(color: .red)
    VerihubsAsset_BCAD.getInstance().setLivenessReminderTextColor(color: .white)
    VerihubsAsset_BCAD.getInstance().setLivenessRulesTextColor(color: .blue)
    VerihubsAsset_BCAD.getInstance().setLivenessProcessingTextColor(color: .gray)
    
    //liveness instructions -- texts
    VerihubsString_BCAD.getInstance().setHeadLookLeftInstruction(headLookLeftInstruction: "Hadap kiri")
    VerihubsString_BCAD.getInstance().setHeadLookRightInstruction(headLookRightInstruction: "Hadap kanan")
    VerihubsString_BCAD.getInstance().setHeadLookStraightInstruction(headLookStraightInstruction: "Hadap depan")
    VerihubsString_BCAD.getInstance().setMouthOpenInstruction(mouthOpenInstruction: "Buka dan tutup mulut")
    
    //liveness warning -- warning
    VerihubsString_BCAD.getInstance().setRemoveMaskWarning(removeMaskWarning: "Lepaskan masker")
    VerihubsString_BCAD.getInstance().setRemoveSunglassesWarning(removeSunglassesWarning: "Lepaskan kacamata")
    VerihubsString_BCAD.getInstance().setBlurDetectedWarning(blurDetectedWarning: "Pastikan smartphone tidak bergoyang")
    VerihubsString_BCAD.getInstance().setDarkDetectedWarning(darkDetectedWarning: "Pastikan berada di ruangan yang cukup terang")
    VerihubsString_BCAD.getInstance().setFaceTooFarWarning(faceTooFarWarning: "Dekatkan wajah")
    
    //liveness bottom -- texts
    VerihubsString_BCAD.getInstance().setFirstRuleText(firstRuleText: "Pastikan seluruh area wajah berada dalam lingkaran")
    VerihubsString_BCAD.getInstance().setSecondRuleText(secondRuleText: "Pastikan posisi camera setinggi mata")
    VerihubsString_BCAD.getInstance().setReminderMessage(reminderMessage: "Selama proses berlangsung seluruh area wajah diharuskan berada dalam lingkaran")
    
    //liveness instructions -- asset
    VerihubsAsset_BCAD.getInstance().setLookLeft(lookLeft: "LeaLookLeft.gif")
    VerihubsAsset_BCAD.getInstance().setLookRight(lookRight: "LeaLookRight.gif")
    VerihubsAsset_BCAD.getInstance().setLookStraight(lookStraight: "Ins-FR_LeaNon-Anim_Straight")
    VerihubsAsset_BCAD.getInstance().setOpenMouth(openMouth: "LeaOpenMouth.gif")
    VerihubsAsset_BCAD.getInstance().setBackButton(backButton: "verihubs_black_back_button")

    //liveness instructions -- warning assets
    VerihubsAsset_BCAD.getInstance().setFirstRule(imageName: "Ins-FR_SteadyPhone")
    VerihubsAsset_BCAD.getInstance().setSecondRule(imageName: "Ins-FR_SteadyPhone")
    
    VerihubsAsset_BCAD.getInstance().setWarningBlur(imageName: "Ins-FR_SteadyPhone")
    VerihubsAsset_BCAD.getInstance().setWarningDark(imageName: "Ins-FR_Turn On Light")
    VerihubsAsset_BCAD.getInstance().setWarningMask(imageName: "Ins-FR_Error_NoMaskHatGlass")
    VerihubsAsset_BCAD.getInstance().setWarningSunglass(imageName: "Ins-FR_Error_NoMaskHatGlass")
    VerihubsAsset_BCAD.getInstance().setWarningFaceTooFar(imageName: "girl_face_in_circle")
    VerihubsAsset_BCAD.getInstance().setWarningProcessing(imageName: "Ins-FR_Processing_Hourglass")
    
    VerihubsAsset_BCAD.getInstance().setLivenessBottomLayoutWarningColor(color: .white)
    VerihubsAsset_BCAD.getInstance().setLivenessBottomLayoutInstructionsColor(color: .white)
    VerihubsAsset_BCAD.getInstance().setLivenessBottomLayoutRulesColor(color: .white)
    VerihubsAsset_BCAD.getInstance().setLivenessBottomLayoutBackground(color: .systemYellow)

    print("instruction_count: " + String(self.instruction_count)) 

    verisdk.verifyLiveness(viewController:self.viewController!, delegate:self,showTutorial: true)
  }

  @objc
  func takeSelfie(_ command: CDVInvokedUrlCommand){
    
    //noliveness -- text color
    VerihubsAsset_BCAD.getInstance().setNolivenessInstructionsTextFaceColor(color: .white)
    VerihubsAsset_BCAD.getInstance().setNolivenessInstructionsTextNoFaceColor(color: .red)
    VerihubsAsset_BCAD.getInstance().setNolivenessRepeatButtonTextColor(color: .blue)
    VerihubsAsset_BCAD.getInstance().setNolivenessContinueButtonTextColor(color: .white)
    VerihubsAsset_BCAD.getInstance().setNolivenessTakeSelfieButtonFaceTextColor(color: .white)
    VerihubsAsset_BCAD.getInstance().setNolivenessTakeSelfieButtonNoFaceTextColor(color: .black)
    
    //noliveness -- container color
    VerihubsAsset_BCAD.getInstance().setNolivenessBackgroundColor(color: .white)
    VerihubsAsset_BCAD.getInstance().setNolivenessButtonBorderRepeatSelfieColor(color: .blue)
    VerihubsAsset_BCAD.getInstance().setNolivenessButtonRepeatSelfieColor(color: .white)
    VerihubsAsset_BCAD.getInstance().setNolivenessButtonContinueColor(color: .blue)
    VerihubsAsset_BCAD.getInstance().setNolivenessButtonTakeSelfieNoFaceColor(color: .gray)
    VerihubsAsset_BCAD.getInstance().setNolivenessButtonTakeSelfieFaceColor(color: .blue)
    
    //noliveness -- circle color
    VerihubsAsset_BCAD.getInstance().setNolivenessInvertedCircleFaceDetected(color: .blue)
    VerihubsAsset_BCAD.getInstance().setNolivenessInvertedCircleFaceNotDetected(color: .red)
    
    //noliveness -- texts
    VerihubsString_BCAD.getInstance().setNoLivenessInstruction(instructionsText: "Pastikan wajahmu terlihat jelas (lepaskan kacamata, topi atau masker)")
    VerihubsString_BCAD.getInstance().setNoLivenessNoFace(text: "Face is not ready")
    VerihubsString_BCAD.getInstance().setNoLivenessContinue(text: "Lanjutkan")
    VerihubsString_BCAD.getInstance().setNoLivenessRepeat(text: "Ulangi")
    
    //noliveness -- asset
    VerihubsAsset_BCAD.getInstance().setNolivenessBackButton(imageName: "verihubs_black_back_button")
    
    verisdk.takeSelfie(viewController:self.viewController!, delegate:self,showTutorial: false)
    
  }

  public func onActivityResult(responseResult response_result: [AnyHashable : Any])
  {
      var pluginResult:CDVPluginResult
      pluginResult = CDVPluginResult.init(status: CDVCommandStatus_OK, messageAs: response_result)
      self.commandDelegate.send(pluginResult, callbackId: self.commandId)

  }

  @objc
  func getVersion(_ command: CDVInvokedUrlCommand) {

    var response_result: [AnyHashable : Any] = [:]

    let temp2 = ["version": "2.5.0"] as [AnyHashable : Any]

    response_result = Array(response_result.keys).reduce(temp2) { (dict, key) -> [AnyHashable:Any] in
        var dict = dict
        dict[key] = response_result[key]
        return dict
    }
    
    var pluginResult:CDVPluginResult
    pluginResult = CDVPluginResult.init(status: CDVCommandStatus_OK, messageAs: response_result)
    self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
  }
}
