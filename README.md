# Verihubs Cordova Plugin

Verihubs Cordova Plugin is our SDK that has been ported to Cordova.

## Functions

There are currently 3 methods in our plugin.

### Initializing Class

To use our plugin, we must first initialize the plugin. If successful it will return a success callback.

### Verify Liveness

You can use our liveness detection method by supplying it a count of how many instructions will be provided and a time limit for finishing the active part of our liveness detection process. This function will return a status code, number of instructions given, the instructions given and images taken during the detection process in Base64 form. The number of instructions and images returned correlates to the number of instruction given and you can access it by appending a number (starting from 1) as shown in our example below.

<aside class="notice">
Status code 200 indicates that the process has run successfully, status code 401 indicates that the active part has not run successfully, status code 402 indicates that the passive part has not run successfully and status code 403 indicates the process has been cancelled.
</aside>

### Get Version

You can check our SDK version by calling this method.


## Installing Cordova Environment iOS
To install the cordova command-line tool, follow these steps:

1. Download and install Node.js. On installation you should be able to invoke `node` and `npm` on your command line.
2. Install the `cordova` module using `npm` utility of Node.js. The `cordova` module will automatically be downloaded by the `npm` utility.<br>
`$ sudo npm install -g cordova`<br>
On OS X and Linux, prefixing the npm command with sudo may be necessary to install this development utility in otherwise restricted directories such as /usr/local/share. If you are using the optional nvm/nave tool or have write access to the install directory, you may be able to omit the sudo prefix.

## Create App
Go to the directory where you maintain your source code, and create a cordova project:<br>
`$ cordova create hello com.example.hello HelloWorld`<br>
By default, the cordova create script generates a skeletal web-based application whose home page is the project's www/index.html file.

## Add Platform
All subsequent commands need to be run within the project's directory, or any subdirectories:<br>
`$ cd hello`
<br>Add the platforms that you want to target your app. We will add the 'ios' platform and ensure they get saved to `config.xml` and `package.json`:<br>
`$ cordova platform add ios`
<br>
## Add Plugin
To add cordova plugin wrapper, follow these steps:
1. Download plugin wrapper here https://gitlab.com/verihubs/cordova-plugin-wrapper. or clone using git below outside the HelloWorld project.<br>
`$ git clone https://gitlab.com/verihubs/cordova-plugin-wrapper`<br>
2. Change directory to your HelloWorld Project.<br>
`$ cd hello`
3. Add new plugin<br>
`$ cordova plugin add ../cordova-plugin-wrapper/`

check registered plugin<br>
`$ cordova plugin list`<br>
also check inside folder `platform/ios/HelloWorld/Plugins`. if it is registered, there will be a new plugin file that has been added.

if you want to update the plugin, remove the plugin that you want to update using this command `$ cordova plugin rm <plugin>`, and then re-add the plugin.

## Configure Interface
1. open file `platform/ios/www/index.html`, you can configure this file for setting the cordova interface using html. create new button for init, liveness and get version inside body tag.<br>
```html
<body>
    <div class="app">
           <button id="init">Init</button>
           <button id="liveness">Liveness</button>
           <button id="getVersion">Version</button>
    </div>
        <script src="cordova.js"></script>
        <script src="js/index.js"></script>
</body>
```
2. Configure javaScript file, open file `platform/ios/www/js/index.js`, add this code.
```html
document.addEventListener('deviceready', onDeviceReady, false);

document.getElementById('init').addEventListener("click",initClick);
document.getElementById('liveness').addEventListener("click",livenessCLick);
document.getElementById('getVersion').addEventListener("click",getVerClick);

function initClick(){
    alert("init");

    window.plugins.verihubssdk.initClass(initSuccess, initError);

    function initSuccess() {
        alert("SDK Initialization success");
    }

    function initError(error) {
        alert(error);
    }
}

function livenessCLick(){
    alert("liveness");

    instruction_count = 2
    timeout = 15000
    custom_instructions = [0, 1]
    check_attributes = [true, true]

    window.plugins.verihubssdk.verifyLiveness(
        instruction_count,
        timeout,
        custom_instructions,
        check_attributes,
        livedetSuccess,
        livedetError
      );

  function livedetSuccess(result) {
      alert(result.status);
      alert(result.total_instruction);
      alert(result.instruction1);
      alert(result.base64String_1);
  }

  function livedetError(error) {
      alert(error);
  }
}

function getVerClick(){
    alert("getVersion")

    window.plugins.verihubssdk.getVersion(getVersionSuccess, getVersionError);

    function getVersionSuccess(result) {
        alert(result.version);
    }

    function getVersionError(error) {
        alert(error);
    }

}

function onDeviceReady() {
    // Cordova is now initialized. Have fun!

    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    document.getElementById('deviceready').classList.add('ready');
}
```

## Build Apps
1. Save all files, and open your ios xcworkspace in folder `platforms/ios`
2. Connect your device to the computer.
3. Build and run the app from xcode.
